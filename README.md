# Fedora Docs Workshop

## Description
Use Linux on the desktop or server? Want to benefit from writing user documentation? There’s no better way to learn about contribution process than with the Fedora Docs workshop.

## How to Join
https://meet.jit.si/moderated/7c16f7cbf93b71acc23ac29c4fbed0372ce4c3066497f12a68488a3d95a9c64d

A monthly agenda will be posted in Fedocal.

https://calendar.fedoraproject.org/docs/

## License
CC BY-SA 4.0